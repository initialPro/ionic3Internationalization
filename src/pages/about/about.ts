import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {TranslateService} from "ng2-translate";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, public translate: TranslateService) {

  }

}
